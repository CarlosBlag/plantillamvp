package com.carlosblag.plantillamvp;

import android.app.Application;
import android.content.res.Configuration;

import com.carlosblag.functional7.dagger.DaggerFunctional7Component;
import com.carlosblag.functional7.dagger.Functional7Component;
import com.carlosblag.functional7.dagger.Functional7Module;
import com.carlosblag.plantillamvp.dagger.NombreAppComponent;
import com.carlosblag.plantillamvp.dagger.NombreAppModule;

/**
 * Created by Duca on 03/12/2017.
 */

public class MiApplication extends Application {

    private NombreAppComponent appComponent;


    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = initDagger(this);

    }

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    //region dagger
    public NombreAppComponent getAppComponent() {
        return appComponent;
    }

    protected NombreAppComponent initDagger(MiApplication application) {
        return DaggerFunctional7Component.builder()
                .functional7Module(new NombreAppModule(application))
                .build();
    }
    //endregion
}
