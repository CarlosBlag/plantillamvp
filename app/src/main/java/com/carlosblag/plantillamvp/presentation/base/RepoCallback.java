package com.carlosblag.plantillamvp.presentation.base;

import retrofit2.Response;

/**
 * Created by Duca on 12/12/2017.
 */

public interface RepoCallback {
    void resultOk(Response response);

    void resultKo(String response);
}
