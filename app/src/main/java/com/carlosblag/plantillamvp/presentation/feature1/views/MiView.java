package com.carlosblag.plantillamvp.presentation.feature1.views;


import com.carlosblag.plantillamvp.domain.models.ErrorWs;
import com.carlosblag.plantillamvp.presentation.base.BaseView;

/**
 * Created by Duca on 10/12/2017.
 */

public interface MiView extends BaseView {

    void loginOkEjemplo(String token, Boolean changePass);

    void loginKoEjemplo(ErrorWs error);

}
