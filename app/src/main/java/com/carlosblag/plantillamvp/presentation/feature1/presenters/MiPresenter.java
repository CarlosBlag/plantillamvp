package com.carlosblag.plantillamvp.presentation.feature1.presenters;

import com.carlosblag.plantillamvp.presentation.base.BasePresenter;
import com.carlosblag.plantillamvp.presentation.feature1.views.MiView;

/**
 * Created by necsia on 27/2/18.
 */

public interface MiPresenter extends BasePresenter<MiView> {

        void loginEjemplo(String email, String password);

        void saveTokenEjemplo(String token);

        }