package com.carlosblag.plantillamvp.presentation.feature1.presenters.impl;

import android.content.Context;

import com.carlosblag.plantillamvp.extras.Constants;
import com.carlosblag.plantillamvp.extras.PreferencesHelper;
import com.carlosblag.plantillamvp.presentation.feature1.presenters.MiPresenter;
import com.carlosblag.plantillamvp.presentation.feature1.views.MiView;


/**
 * Created by Duca on 10/12/2017.
 */

public class MiPresenterImpl implements MiPresenter {

    private final PreferencesHelper preferencesHelper;
    private MiUseCase mLoginUseCase;
    private MiView view;
    private MiUseCaseImpl mLoginUseCaseImpl;

    public MiPresenterImpl(MiUseCase loginUseCase,
                           Context context) {
        mLoginUseCase = loginUseCase;
        preferencesHelper = new PreferencesHelper(context);

    }

    public void login(String email, String password) {
        mLoginUseCaseImpl = (MiUseCaseImpl) mLoginUseCase;
        mLoginUseCaseImpl.login(email, password, new MiCallback() {
            @Override
            public void loginOk(String token, boolean changePass) {
                if (view != null) {
                    view.hideProgress();
                    saveToken(token);
                    view.loginOk(token, changePass);
                }
            }

            @Override
            public void loginKo(String string) {
                if (view != null) {
                    view.hideProgress();
                    view.showError(string);
                }
            }
        });
    }

    public void saveToken(String token) {
        preferencesHelper.savePref(Constants.TOKEN_OAUTH, token);
        preferencesHelper.savePref(Constants.LOGED, true);
    }

    @Override
    public void destroy() {
        view = null;
        mLoginUseCase = null;
    }

    @Override
    public void setView(MiView view) {
        this.view = view;
    }
}
