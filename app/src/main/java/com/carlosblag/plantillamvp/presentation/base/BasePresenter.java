package com.carlosblag.plantillamvp.presentation.base;

/**
 * Created by Duca on 10/12/2017.
 */

public interface BasePresenter<T extends BaseView> {
    void destroy();

    void setView(T var1);
}
