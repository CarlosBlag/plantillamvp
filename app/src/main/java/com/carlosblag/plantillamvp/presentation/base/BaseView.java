package com.carlosblag.plantillamvp.presentation.base;

/**
 * Created by Duca on 10/12/2017.
 */

public interface BaseView {
    void showProgress();

    void hideProgress();

    void showError(String errorWs);
}
