package com.carlosblag.plantillamvp.extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;


import com.carlosblag.plantillamvp.R;

import java.util.Set;

public class PreferencesHelper {

    public static final String PREF_AUTH_TOKEN = "authToken";


    private final SharedPreferences mSharedPreferences;
    private final SharedPreferences.Editor mEditor;


    public PreferencesHelper(Context context) {
        String preferencesFile = context.getString(R.string.prefs_name);
        mSharedPreferences = context.getSharedPreferences(preferencesFile, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void delete(String key) {
        if (mSharedPreferences.contains(key)) {
            mEditor.remove(key).commit();
        }
    }

    public void savePrefWithoutDelete(String key, Object value) {
        savePrefValue(key, value);
    }

    public void savePref(String key, Object value) {
        delete(key);
        savePrefValue(key, value);
    }

    private void savePrefValue(String key, Object value) {
        if (value instanceof Boolean) {
            mEditor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            mEditor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            mEditor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            mEditor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            mEditor.putString(encrypt(key), encrypt((String) value));
        } else if (value instanceof Enum) {
            mEditor.putString(key, value.toString());
        } else if (value instanceof Set) {
            mEditor.putStringSet(key, (Set<String>) value);
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-primitive preference");
        }

        mEditor.commit();
    }

    @SuppressWarnings("unchecked")
    public <T> T getPref(String key) {
        return (T) mSharedPreferences.getAll().get(key);
    }

    public <T> T getPrefString(String key) {
        return (T) decrypt(mSharedPreferences.getString(encrypt(key), encrypt("default")));
    }

    @SuppressWarnings("unchecked")
    public <T> T getPref(String key, T defValue) {
        T returnValue = (T) mSharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public boolean isPrefExists(String key) {
        return mSharedPreferences.contains(key);
    }

    public static String encrypt(String input) {
        // This is base64 encoding, which is not an encryption
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
