package com.carlosblag.plantillamvp.extras;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Duca on 10/12/2017.
 */

public class Utils {
    private final Context mContext;

    public Utils(Context context) {
        mContext = context;
    }

    public void showErrorToast(String errorMessage) {
        Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
    }

    public boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 2;
    }
}
