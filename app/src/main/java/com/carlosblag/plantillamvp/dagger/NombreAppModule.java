package com.carlosblag.plantillamvp.dagger;

/**
 * Created by Duca on 10/12/2017.
 */

import android.content.Context;

import com.carlosblag.plantillamvp.MiApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NombreAppModule {
    MiApplication mApplication;

    public NombreAppModule(MiApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }


}
