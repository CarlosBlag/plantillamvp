package com.carlosblag.plantillamvp.dagger;



import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Duca on 10/12/2017.
 */

@Singleton
@Component(modules = {NombreAppModule.class,
        PresenterModule.class,
        RepositoryModule.class,
        UseCaseModule.class})
public interface NombreAppComponent {
    //añadir las activities y fragments necesarios
    void inject(MainActivity activity);

}
