package com.carlosblag.plantillamvp.dagger;

import android.content.Context;

import com.carlosblag.functional7.features.login.repositories.impl.ChangePassRepositoryImpl;
import com.carlosblag.functional7.features.login.repositories.impl.LoginRepositoryImpl;
import com.carlosblag.functional7.features.videos.repositories.impl.GetVideosRepositoryImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Duca on 10/12/2017.
 */

@Module
public class RepositoryModule {

    @Provides
    LoginRepositoryImpl provideLoginRepository(Context context) {
        return new LoginRepositoryImpl(context);
    }

    @Provides
    ChangePassRepositoryImpl providesChangePassRepository(Context context) {
        return new ChangePassRepositoryImpl(context);
    }

    @Provides
    GetVideosRepositoryImpl providesGetVideosRepository(Context context) {
        return new GetVideosRepositoryImpl(context);
    }
}
