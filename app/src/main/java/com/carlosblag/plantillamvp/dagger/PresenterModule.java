package com.carlosblag.plantillamvp.dagger;

import android.content.Context;


import com.carlosblag.plantillamvp.presentation.feature1.presenters.MiPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Duca on 10/12/2017.
 */

@Module
public class PresenterModule {
    //añadir los presenters necesarios
    @Provides
    MiPresenter provideLoginPresenter(Context context,
                                      MiUseCase loginUseCase) {
        return new MiPresenterImpl(loginUseCase, context);
    }


}
