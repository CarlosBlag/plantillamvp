package com.carlosblag.plantillamvp.dagger;


import dagger.Module;
import dagger.Provides;

/**
 * Created by Duca on 10/12/2017.
 */

@Module
public class UseCaseModule {

    @Provides
    MiUseCase provideLoginUseCase(MiRepositoryImpl loginRepository) {
        return new MiUseCaseImpl(loginRepository);
    }
}
