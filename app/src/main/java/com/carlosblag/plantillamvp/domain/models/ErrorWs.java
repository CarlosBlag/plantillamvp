package com.carlosblag.plantillamvp.domain.models;

/**
 * Created by Duca on 10/12/2017.
 */

public class ErrorWs {
    String errorMessage;
    String errorCode;

    public ErrorWs(String errorMessage, String errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
